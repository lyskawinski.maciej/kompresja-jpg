from functools import reduce

import numpy as np
from PIL import Image, ImageChops
import cv2
import os
import plotly.express as px

compressions = [5, 10, 30, 50, 70, 80, 90, 100]
for image_name in os.listdir("./images"):
    image = Image.open(os.path.join(".", "images", image_name))
    y = []
    for compression in compressions:
        image.save("tmp", "JPEG", quality=100 - compression)

        diff = np.array(ImageChops.difference(image, Image.open("tmp")))
        size = reduce(lambda a,b:a*b,diff.shape)*256
        y.append(sum(diff.flatten())/size)

    fig = px.line({"compression": compressions, "difference": y}, x="compression", y="difference", title=image_name)
    fig.show()
